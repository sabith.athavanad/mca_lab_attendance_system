<?php
   session_start();
   if($_SESSION["login_admin"]) {
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Admin Home</title>
    
    <!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: "Lato", sans-serif;
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


  
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    align:center;
    width:80%; 
    margin-left:10%;
     margin-right:10%;
  }
  .td-non{
    border: 0px;
  }
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  .td-align{
    text-align: center;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }


  .button {
    border-radius: 4px;
  background-color: blue;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 10px;
  padding: 10px;
  width: 75px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}



  </style>

</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="adminHome.php">Attendance Monitoring</a>
  <a href="createPost.php">Create Post</a>
  <a href="UserReg.php">Add User</a>
  <a href="absentees.php">Hourly Absentees</a>
  
  <a href='adminLogout.php'>Logout</a>
</div>


<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
   
<h2 style="text-align: center;">Monitoring Session</h2>
<table>
  <tr>
  <form method="post" action="">
    <td class="td-non" style="text-align: center;">Select the time period : <input type="time" name="time">
    <input type="date" name="date">
    <button type="submit" name="show">Show </button></td></form>
  </tr>
</table>
<br/></br/>
<table>
  <tr>
    <th>Roll No.</th>
    <th>Student Name</th>
    <th>Date</th>
    <th>Time IN </th>
    <th>Time Out </th>
    <th>System No. </th>
</tr>
<?php
if(isset($_POST['show'])){ 

        
  $time = $_POST['time'];
  $date_today = $_POST['date'];
  
include("config.php");
$sql = "SELECT roll, name, date, time_in,time_out, system FROM test where time_in > '$time' and date='$date_today'";
$result = mysqli_query($db, $sql);

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
      $date2 = $row["time_in"];
      $roll = $row["roll"];
      $name = $row["name"];
      $system = $row["system"];
      $date1 = $row["date"];
      $date3 = date('h:i:s a', strtotime($date2));
        echo "
          <tr>
            <td>" . $row["roll"]. "</td>
            <td width=300>" . $row["name"]. "</td>
            <td>" . $row["date"]. "</td>
            <td>" . $date3. "</td>
            <td>" . $row["time_out"]. "</td>
            <td class=td-align>" . $row["system"]. "</td>
            <td><a href=viewUser.php?roll=$roll&system=$system&date=$date1&name=$name><button type=submit class=button><span>View</span></button></a></tr>";
            
    }
} else {
    echo "No data is found in <b><font color = red> $time and $date_today</b></font> Period";
}  
}
?>
</table>

 

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <?php
}else header("location: adminLogin.php");
?>
  </body>
</html>
