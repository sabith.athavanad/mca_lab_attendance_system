<?php
   session_start();
   if($_SESSION["login_admin"]) {
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Admin Home</title>
    
    <!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: "Lato", sans-serif;
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


  
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    align:center;
    width:80%; 
    margin-left:10%;
     margin-right:10%;
  }
  .td-non{
    border: 0px;
  }
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  .td-align{
    text-align: center;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }


  .button {
    border-radius: 4px;
  background-color: blue;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 10px;
  padding: 10px;
  width: 75px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}



  </style>

</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="adminHome.php">Attendance Monitoring</a>
  <a href="createPost.php">Create Post</a>
  <a href="UserReg.php">Add User</a>
  <a href="absentees.php">Hourly Absentees</a>
  <a href="report.php">Absentees Report</a>
  <a href='adminLogout.php'>Logout</a>
</div>


<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
   
<h2 style="text-align: center;">Individual user Session</h2>
<table>
  <tr>
  <form method="post" action="">
    
  </tr>
</table>
<br/></br/>
<table>
<tr>
    <td>Roll No.</td>
    <td><?php echo $_GET["roll"]; ?></td>
</tr>
<tr>
    <td>Name</td>
    <td><?php echo $_GET["name"]; ?></td>

</tr>
<tr>
    <td>System Number</td>
    <td><?php echo $_GET["system"]; ?></td>

</tr>
<tr>
    <form action="" method="post">
    <td>Enter your comments to the user</td>
    <td><input type="text" name="comment" placeholder="Comments" rows=5 cols=20><button type="submit" name="send">send </button></td>
</form>
</tr>
<?php
if(isset($_POST['send'])){ 

        
  $comment = $_POST['comment'];
  $roll = $_GET["roll"];
  $date = $_GET["date"];
  
include("config.php");
$sql = "update test set message='$comment' where roll='$roll' and date='$date'";
$result = mysqli_query($db, $sql);
echo "<script>
alert('Succesfully message sent to the user');
window.location.href='adminHome.php';
</script>";


}
?>
</table>

 

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <?php
}else header("location: adminLogin.php");
?>
  </body>
</html>
