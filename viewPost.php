<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>View Posts | AMS</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  
  <!-- Vendor CSS Files -->
  <link href="css1/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="css1/icofont/icofont.min.css" rel="stylesheet">
  <link href="css1/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="css1/venobox/venobox.css" rel="stylesheet">
  <link href="css1/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="css1/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="css1/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Flexor - v2.3.0
  * Template URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body background="img/userpage.jpg">

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="cta">
        <a href="#about" class="scrollto">Create the Posts</a>
      </div>
      <!--<div class="contact-info mr-auto">
        <ul>
          <li><i class="icofont-envelope"></i><a href="mailto:contact@example.com">AMS</a></li>
          <li><i class="icofont-phone"></i> +1 5589 55488 55</li>
          <li><i class="icofont-clock-time icofont-flip-horizontal"></i> Mon-Fri 9am - 5pm</li>
        </ul>

      </div>
      <div class="cta">
        <a href="#about" class="scrollto">Get Started</a>
      </div>-->
    </div>
  </section>

 


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        

        <div class="row justify-content-center">

          

          <!--<div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="100">
            <div class="info-box">
              <i class="bx bx-envelope"></i>
              <h3>Email Us</h3>
              <p>info@example.com<br>contact@example.com</p>
            </div>
          </div> -->
          
        </div>
    <?php
    include("config.php");
    $class= $_COOKIE["class"];
    $sql = "SELECT topic, deadline, instructions, content FROM posts where class='$class' order by id desc";
    $result = mysqli_query($db, $sql);
    
    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            //echo $row["topic"]; }}

    echo "
        <div class=row justify-content-center data-aos=fade-up data-aos-delay=300>
          <div class=col-xl-9 col-lg-12 mt-4>
            <form>
              <div class=form-row>
                <div class=col-md-6 form-group>
                  <label name=topic class=form-control id=name><b><font size=5>". $row["topic"]. "</font></b></label>
                  <div class=validate></div>
                </div>
                <div class=col-md-6 form-group>
                  <label class=form-control name=deadline >Deadline: <b>". $row["deadline"].  "</b></label>
                  <div class=validate></div>
                </div>
              </div>
              <div class=form-group>
                <label class=form-control name=instructions>".$row["instructions"]. "</label>
                <div class=validate></div>
              </div>
              <div class=form-group>
                <textarea class=form-control name=content rows=8>". $row["content"]."</textarea>
                <div class=validate></div>
              </div>
              
              <br/><br/><hr><br/><br/>
              
            </form>
          </div>

        </div>";
        }}
?>
      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  

    

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="css1/jquery/jquery.min.js"></script>
  <script src="css1/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="css1/jquery.easing/jquery.easing.min.js"></script>
  <script src="css1/php-email-form/validate.js"></script>
  <script src="css1/jquery-sticky/jquery.sticky.js"></script>
  <script src="css1/venobox/venobox.min.js"></script>
  <script src="css1/owl.carousel/owl.carousel.min.js"></script>
  <script src="css1/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="css1/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="css1/main.js"></script>

</body>


</html>